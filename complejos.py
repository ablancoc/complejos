class Complejo:
    def __init__(self, r, i):
        self.real = r
        self.img = i

"Esto es una funcion que devuelve un resultado con forma de complejo si  modificar los valores que tenías de antes. esta se asemeja a c = a + b"
    @staticmethod
    def sumar1(n1,n2):
        r = n1.real + n2.real
        i = n1.img + n2.img
        resultado = Complejo(r,i)
        return resultado

    def sumar2(self, n1, n2):
        pass

"Esto es un metodo que suma los valores y solo te guarda el valor sumado. Esta se asemeja a a+=b."
    def suma_me(self, n2):
        self.real= self.real + n2.real
        self.img = self.img + n2.img

"Esto te cambia los valores y te devuelve el resultado, por lo que lo puedes usar pasar otra cosa después"
    def suma_me2(self,n2):
        self.real= self.real + n2.real
        self.img = self.img + n2.img
        return self
        
    def __str__(self):
        return str(self.real) + "+" + str(self.img) + "j"
        
        
v1 = Complejo(3,2)
v2 = Complejo(5,4)

v1.suma_me(v2)